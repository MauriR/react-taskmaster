import React from 'react'

type Props = {
    type: 'submit' | 'reset' | 'button',
    label: string
}

const Button: React.FC<Props> = (props: Props) => {
    return <button
        className="input__submit"
        type={props.type}
        >
        {props.label}
       
    </button>
}

export default Button
