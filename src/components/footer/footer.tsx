import React from 'react'
import LanguageSelector from './languageSelector'

const Footer = ()=>{

    return <div>
        <LanguageSelector/>
    </div>
}

export default Footer