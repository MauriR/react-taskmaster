import React from 'react'
import Selector, { Option } from '../genericSelector'
import { useTranslation } from "react-i18next"


const Language = () => {
    const { i18n, t } = useTranslation()
    const languages: Array<Option> = [
        { name: "english", value: "en" },
        { name: "spanish", value: "es" }
    ]

    const changeLanguage = (code: string) => {
        i18n.changeLanguage(code)
    }

    const translated = (languages: Array<Option>): Array<Option> => {
        return languages.map((option: Option) => {
            return { name: t(option.name), value: option.value }
        })
    }



    return <Selector options={translated(languages)} selected={changeLanguage} />
}

export default Language