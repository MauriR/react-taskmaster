import React from 'react'

export type Option = {
    name: string
    value: string
}

interface GenericSelectorProps {
    options: Array<Option>,
    selected: (value: string) => void
}


const Selector: React.FC<GenericSelectorProps> = ({ options, selected }: GenericSelectorProps) => {

    return <div>
        <select onChange={(event) => selected(event.target.value)}>
            {options.map((option: Option) =>
                <option key={option.value} value={option.value}>{option.name}</option>)}
        </select>
    </div>
}

export default Selector




