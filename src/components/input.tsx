import React from 'react'

export type Props = { 
    type:string,
    value: string,
  
    placeholder: string,
    className:string     
    
}
interface InputProps  {
    type:string,
    value: string,
    
    placeholder: string,
    className:string  
    writen : (value:string)=> void
}


const Input: React.FC<InputProps> = ({ type, value,  className, placeholder, writen} :InputProps)=>{
    return <input 
    onChange={(event) => writen(event.target.value)}
    value={value}
    type={type}
    placeholder={placeholder}
    className={className}
    />
}

export default Input
