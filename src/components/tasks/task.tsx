import React, { useState } from 'react'
import "../styles.css"
import { useTranslation } from "react-i18next"
import Button from '../button'
import Input from '../input'
import {TaskType }from '../../types/task'

interface Props {
  todo: string
  setTodo: React.Dispatch<React.SetStateAction<string>>
  add:(event: React.FormEvent<HTMLFormElement>)=> void;
}

const Task: React.FC<Props> = ({ todo, setTodo, add }) => {
  const { t } = useTranslation()
  const placeholder = t('taskPlaceholder')
  const buttonLabel = t('go')
  // let tasks:any= []
 
  const printInput = (e: string): void => {
    console.log('Something has been writen in task input', e);
    setTodo(e)
  }
  // const create = (event: React.FormEvent<HTMLFormElement>) => {
  //   event.preventDefault()
  //   tasks.push(todo)
    
     
  //   alert(tasks);
  //   setTodo('')
  // }
  


  return (
    <form className="input" onSubmit={(event) => add(event)}>
      <Input
        value={todo}
        type="text"
        writen={printInput}
        className="input__box"
        placeholder={placeholder}
      />
      
      <Button label={buttonLabel} type="submit" />
    </form>


  )
}

export default Task