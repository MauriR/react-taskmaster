export interface TaskType{
    id: string,
    todo: string,
    isDone: boolean
}