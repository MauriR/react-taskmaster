import React, { useState } from 'react';
import './App.css';
import Task from './components/tasks/task';
import Footer from './components/footer/footer'
import { useTranslation } from "react-i18next"
import { TaskType } from './types/task'

const App: React.FC = () => {
  const [todo, setTodo] = useState<string>("")
  const [tasks, setTasks] = useState<TaskType[]>([])
  const { t } = useTranslation()
  const buttonLabel = t('go')

  const add = (event: React.FormEvent) => {
    event.preventDefault()
    if (todo) {
      setTasks([...tasks, { id: Date.now().toString(), todo, isDone: false }])
      setTodo("")
    }
  }
  return (
    <div className="App">
      <span className="heading">Taskmaster</span>
      <Task todo={todo} setTodo={setTodo} add={add} />
      {tasks.map((t)=>(<li key={t.id}>{t.todo}</li>))}
      <Footer />
    </div>
  );
}

export default App;
